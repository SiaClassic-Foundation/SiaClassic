#!/usr/bin/env bash
apt-get update
apt-get upgrade -y
wget https://dl.google.com/go/go1.10.4.linux-amd64.tar.gz
tar -xvf go1.10.4.linux-amd64.tar.gz
echo "export GOROOT=/$USER/go" > ~/.bashrc
echo "export GOPATH=/$USER/gocode" > ~/.bashrc
echo "export PATH=$PATH:$GOROOT/bin:$GOPATH/bin" > ~/.bashrc
exec bash
chown -R root:root ./go
if [ ! -d gocode ]; then
  mkdir -p gocode;
fi
go get -u gitlab.com/SiaClassic-Foundation/SiaClassic...
siad -h